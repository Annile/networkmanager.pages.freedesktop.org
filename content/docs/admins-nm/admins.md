---
title: "Info for Admins"
date:
weight: 4
summary: "Information for Admins"
---

#### Architecture

[NetworkManager]("https://developer.gnome.org/NetworkManager/stable/NetworkManager.conf.html").conf is the configuration file of NM. A default configuration file is created by your distribution's packages and shouldn't be modified, because your changes may get overwritten by package updates. Instead, additional .conf files can be added to the ```/etc/NetworkManager/conf.d``` directory. You should check out more [information]("https://developer.gnome.org/NetworkManager/stable/NetworkManager.conf.html") on how NetworkManager integrates into your distribution's normal configuration system.



#### Deployment

[NetworkManager]("https://developer.gnome.org/NetworkManager/stable/NetworkManager.html") is designed to be fully automatic by default. It manages the primary network connection and other network interfaces, like Ethernet, Wi-Fi, and Mobile Broadband devices. To use NetworkManager, its service must be started. Starting up NetworkManager depends on the distribution you are running, but NetworkManager ships with systemd service files to do this for most distributions. NetworkManager will then automatically start other services as it requires them (nm-system-settings for system-wide connections, wpa_supplicant for WPA and 802.1x connections, and pppd for mobile broadband).


#### Security

NetworkManager is designed to be secure, but that does not obsolete standard network security practices. Security must be implemented in each network. NetworkManager supports most network security methods and protocols, including WEP, WPA/WPA2, WPA-Enterprise and WPA2-Enterprise, wired 802.1x, and VPNs. NetworkManager stores network secrets (encryption keys, login information) using secure storage, either in the user's keyring (for user-specific connections) or protected by normal system administrator permissions (like root) for system-wide connections. Various network operations can be locked down with [PolicyKit]("https://www.freedesktop.org/software/polkit/docs/latest/") for even finer grained control over network connections. 


#### VPN

NetworkManager has pluggable support for VPN software, including Cisco compatible VPNs (using vpnc), openvpn, and Point-to-Point Tunneling Protocol (PPTP). Support for other vpn clients is welcomed. Simply install the NetworkManager VPN plugin your site uses, and pre-load the user's machines with the VPN's settings. The first time they connect, the user will be asked for their passwords. 